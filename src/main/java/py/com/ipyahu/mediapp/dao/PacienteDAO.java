package py.com.ipyahu.mediapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import py.com.ipyahu.mediapp.model.Paciente;

public interface PacienteDAO extends JpaRepository<Paciente, Integer>{

}

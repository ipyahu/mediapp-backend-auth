package py.com.ipyahu.mediapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import py.com.ipyahu.mediapp.model.Especialidad;

public interface EspecialidadDAO extends JpaRepository<Especialidad, Integer>{

}

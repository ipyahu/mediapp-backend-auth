package py.com.ipyahu.mediapp.dao.security;
import org.springframework.data.jpa.repository.JpaRepository;

import py.com.ipyahu.mediapp.model.security.Usuario;

public interface UsuarioDAO extends JpaRepository<Usuario, Long> {

	Usuario findByUsername(String username);
}

package py.com.ipyahu.mediapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import py.com.ipyahu.mediapp.model.ConsultaExamen;

public interface ConsultaExamenDAO extends JpaRepository<ConsultaExamen, Integer>{

	//@Transactional
	@Modifying //Esta anotacion nos permite crear sentencias DML usando @Query
	//DML | insert, update, delete |SQL select
	@Query(value="INSERT INTO consulta_examen(id_consulta, id_examen) VALUES (:idConsulta, :idExamen)", nativeQuery=true)
	Integer registrar(@Param("idConsulta") Integer idConsulta, @Param("idExamen") Integer idExamen);
}

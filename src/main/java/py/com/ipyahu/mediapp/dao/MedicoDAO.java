package py.com.ipyahu.mediapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import py.com.ipyahu.mediapp.model.Medico;

public interface MedicoDAO extends JpaRepository<Medico, Integer>{

}

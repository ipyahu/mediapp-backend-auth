package py.com.ipyahu.mediapp.service;

import py.com.ipyahu.mediapp.model.Paciente;

public interface PacienteService extends CRUD<Paciente>{
	//En esta interface podemos definir metodos que no sean propios de la interface CRUD 
}

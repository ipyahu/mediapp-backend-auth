package py.com.ipyahu.mediapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.ipyahu.mediapp.dao.MedicoDAO;
import py.com.ipyahu.mediapp.model.Medico;
import py.com.ipyahu.mediapp.service.MedicoService;

@Service
public class MedicoServiceImpl implements MedicoService{

	@Autowired
	private MedicoDAO dao;
	
	@Override
	public Medico registrar(Medico obj) {
		return dao.save(obj);
	}

	@Override
	public Medico modificar(Medico obj) {
		return dao.save(obj);
	}

	@Override
	public List<Medico> listar() {
		return dao.findAll();
	}

	@Override
	public Medico listarId(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public void eliminar(Integer id) {
		dao.delete(id);
	}

}

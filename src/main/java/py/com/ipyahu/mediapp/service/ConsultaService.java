package py.com.ipyahu.mediapp.service;

import py.com.ipyahu.mediapp.dto.ConsultaListaExamenDTO;
import py.com.ipyahu.mediapp.model.Consulta;

public interface ConsultaService extends CRUD<Consulta>{
	
	Consulta registrar(ConsultaListaExamenDTO consultaDTO);
	
}

package py.com.ipyahu.mediapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import py.com.ipyahu.mediapp.dao.ConsultaDAO;
import py.com.ipyahu.mediapp.dao.ConsultaExamenDAO;
import py.com.ipyahu.mediapp.dto.ConsultaListaExamenDTO;
import py.com.ipyahu.mediapp.model.Consulta;
import py.com.ipyahu.mediapp.service.ConsultaService;

@Service
public class ConsultaServiceImpl implements ConsultaService{

	@Autowired
	private ConsultaDAO dao;
	
	@Autowired
	private ConsultaExamenDAO ceDAO;
	
	@Override
	public Consulta registrar(Consulta obj) {
		return dao.save(obj);
	}

	@Override
	public Consulta modificar(Consulta obj) {
		return dao.save(obj);
	}

	@Override
	public List<Consulta> listar() {
		return dao.findAll();
	}

	@Override
	public Consulta listarId(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public void eliminar(Integer id) {
		dao.delete(id);
	}

	@Transactional
	@Override
	public Consulta registrar(ConsultaListaExamenDTO consultaDTO) {
		consultaDTO.getConsulta().getDetalleConsulta().forEach(d -> d.setCons(consultaDTO.getConsulta()));
		dao.save(consultaDTO.getConsulta());
		consultaDTO.getLstExamen().forEach(e -> ceDAO.registrar(consultaDTO.getConsulta().getIdConsulta(), e.getIdExamen()));
		
		return consultaDTO.getConsulta();
	}

}

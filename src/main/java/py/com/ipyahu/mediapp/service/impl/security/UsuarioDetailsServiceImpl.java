package py.com.ipyahu.mediapp.service.impl.security;
import static java.util.Collections.emptyList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import py.com.ipyahu.mediapp.dao.security.UsuarioDAO;
import py.com.ipyahu.mediapp.model.security.Usuario;

@Service
public class UsuarioDetailsServiceImpl implements UserDetailsService {

	private UsuarioDAO usuarioRepository;

	public UsuarioDetailsServiceImpl(UsuarioDAO usuarioRepository) {
		this.usuarioRepository = usuarioRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuarioRepository.findByUsername(username);
		if (usuario == null) {
			throw new UsernameNotFoundException(username);
		}
		return new User(usuario.getUsername(), usuario.getPassword(), emptyList());
	}
}

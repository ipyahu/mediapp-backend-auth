package py.com.ipyahu.mediapp.service;

import java.util.List;

public interface CRUD<T> {
	public T registrar(T obj);
	public T modificar(T obj);
	public List<T> listar();
	public T listarId(Integer id);
	public void eliminar(Integer id);
}

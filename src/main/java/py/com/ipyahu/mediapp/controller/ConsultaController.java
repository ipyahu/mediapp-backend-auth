package py.com.ipyahu.mediapp.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.com.ipyahu.mediapp.dto.ConsultaDTO;
import py.com.ipyahu.mediapp.dto.ConsultaListaExamenDTO;
import py.com.ipyahu.mediapp.exception.ModeloNotFoundException;
import py.com.ipyahu.mediapp.model.Consulta;
import py.com.ipyahu.mediapp.service.ConsultaService;

@RestController
@RequestMapping("/consultas")
@Api(value="Servicio REST para consultas")
public class ConsultaController {

	@Autowired
	private ConsultaService service;
	
	@ApiOperation("Retorna una lista de Consultas")
	@GetMapping
	public ResponseEntity<List<Consulta>> listar(){
		return new ResponseEntity<List<Consulta>>(service.listar(), HttpStatus.OK);
	}
	
	@ApiOperation("Retorna una lista de Consultas implementando un nivel de madurez 3 con la dependencia HATEOAS")
	@GetMapping(value = "/hateoas", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ConsultaDTO> listarHateoas() {
		List<Consulta> consultas = new ArrayList<>();
		List<ConsultaDTO> consultasDTO = new ArrayList<>();
		consultas = service.listar();

		for (Consulta c : consultas) {
			ConsultaDTO d = new ConsultaDTO();
			d.setIdConsulta(c.getIdConsulta());
			d.setMedico(c.getMedico());
			d.setPaciente(c.getPaciente());

			ControllerLinkBuilder linkTo = linkTo(methodOn(ConsultaController.class).listarId((c.getIdConsulta())));
			d.add(linkTo.withSelfRel());
			consultasDTO.add(d);

			ControllerLinkBuilder linkTo1 = linkTo(methodOn(PacienteController.class).listarId((c.getPaciente().getIdPaciente())));
			d.add(linkTo1.withSelfRel());
			consultasDTO.add(d);

			ControllerLinkBuilder linkTo2 = linkTo(methodOn(MedicoController.class).listarId((c.getMedico().getIdMedico())));
			d.add(linkTo2.withSelfRel());
			consultasDTO.add(d);
		}
		return consultasDTO;
	}
	
	@ApiOperation("Retorna un consulta por su ID")
	@GetMapping(value = "/{id}")
	public Resource<Consulta> listarId(@PathVariable("id") Integer id) {
		Consulta cons = service.listarId(id);
		if (cons == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}
		
		Resource<Consulta> resource = new Resource<Consulta>(cons);
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarId(id));
		resource.add(linkTo.withRel("Consulta-resource"));
		
		return resource;
	}	
	
	@ApiOperation("Permite registrar una consulta")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registrar(@Valid @RequestBody ConsultaListaExamenDTO consultaDTO){
		Consulta cons = new Consulta();
		cons = service.registrar(consultaDTO);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(cons.getIdConsulta()).toUri();
		return ResponseEntity.created(location).build();		
	}
	
	@ApiOperation("Permite modificar una consulta")
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> actualizar(@Valid @RequestBody Consulta Consulta) {		
		service.modificar(Consulta);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@ApiOperation("Permite eliminar una consulta")
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id) {
		Consulta cons = service.listarId(id);
		if (cons == null) {
			throw new ModeloNotFoundException("ID: " + id);
		} else {
			service.eliminar(id);
		}
	}	
}

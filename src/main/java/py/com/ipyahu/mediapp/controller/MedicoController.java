package py.com.ipyahu.mediapp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.com.ipyahu.mediapp.exception.ModeloNotFoundException;
import py.com.ipyahu.mediapp.model.Medico;
import py.com.ipyahu.mediapp.service.MedicoService;

@RestController
@RequestMapping("/medicos")
@Api(value="Servicio REST para medicos")
public class MedicoController {

	@Autowired
	private MedicoService service;
	
	@ApiOperation("Retorna una lista de medicos.")
	@GetMapping(produces= MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<List<Medico>> listar (){
		return new ResponseEntity<List<Medico>>(service.listar(), HttpStatus.OK);
	}
	
	@ApiOperation("Retorna un medico por su ID.")
	@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Medico> listarId(@PathVariable("id") Integer id){
		Medico obj = service.listarId(id);
		if (obj == null){
			throw new ModeloNotFoundException("ID: " + id);
		}
		return new ResponseEntity<Medico>(service.listarId(id), HttpStatus.OK);
	}
	
	@ApiOperation("Permite el registro de un medico.")
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Medico> registrar(@Valid @RequestBody Medico obj){
		return new ResponseEntity<Medico>(service.registrar(obj), HttpStatus.CREATED);
	}
	
	@ApiOperation("Permite la modificacion de un medico.")
	@PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Medico> modificar (@Valid @RequestBody Medico obj){
		return new ResponseEntity<Medico>(service.modificar(obj), HttpStatus.OK);
	}
	
	@ApiOperation("Permite la eliminación de un medico.")
	@DeleteMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable("id") Integer id){
		Medico obj = service.listarId(id);
		if (obj == null){
			throw new ModeloNotFoundException("ID: " + id);
		}
		else{
			service.eliminar(id);
		}
	}
}

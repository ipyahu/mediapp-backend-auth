package py.com.ipyahu.mediapp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.com.ipyahu.mediapp.exception.ModeloNotFoundException;
import py.com.ipyahu.mediapp.model.Especialidad;
import py.com.ipyahu.mediapp.service.EspecialidadService;

@RestController
@RequestMapping("/especialidades")
@Api(value="Servicio REST para especialidades")
public class EspecialidadController {

	@Autowired
	private EspecialidadService service;
	
	@ApiOperation("Retorna una lista de especialidades.")
	@GetMapping(produces= MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<List<Especialidad>> listar (){
		return new ResponseEntity<List<Especialidad>>(service.listar(), HttpStatus.OK);
	}
	
	@ApiOperation("Retorna una especialidad por su ID.")
	@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Especialidad> listarId(@PathVariable("id") Integer id){
		Especialidad obj = service.listarId(id);
		if (obj == null){
			throw new ModeloNotFoundException("ID: " + id);
		}
		return new ResponseEntity<Especialidad>(service.listarId(id), HttpStatus.OK);
	}
	
	@ApiOperation("Permite el registro de una especialidad.")
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Especialidad> registrar(@Valid @RequestBody Especialidad obj){
		return new ResponseEntity<Especialidad>(service.registrar(obj), HttpStatus.CREATED);
	}
	
	@ApiOperation("Permite la modificacion de una especialidad.")
	@PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Especialidad> modificar (@Valid @RequestBody Especialidad obj){
		return new ResponseEntity<Especialidad>(service.modificar(obj), HttpStatus.OK);
	}
	
	@ApiOperation("Permite la eliminación de una especialidad.")
	@DeleteMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable("id") Integer id){
		Especialidad obj = service.listarId(id);
		if (obj == null){
			throw new ModeloNotFoundException("ID: " + id);
		}
		else{
			service.eliminar(id);
		}
	}
}

package py.com.ipyahu.mediapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description="Informacion de la Especialidad")
@Entity
@Table(name="especialidades")
public class Especialidad {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Integer idEspecialidad;
	
	@ApiModelProperty(notes="Nombre de la especialidad puede tener hasta 50 caracteres.")
	@Size(max=50,message="Nombre de la especialidad puede tener hasta 50 caracteres.")
	@Column(name="nombre",nullable= false, length=50)
	private String nombre;
	
	public Integer getIdEspecialidad() {
		return idEspecialidad;
	}
	public void setIdEspecialidad(Integer idEspecialidad) {
		this.idEspecialidad = idEspecialidad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}	
}

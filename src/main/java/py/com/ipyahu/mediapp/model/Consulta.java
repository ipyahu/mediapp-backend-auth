package py.com.ipyahu.mediapp.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name="consultas")
public class Consulta {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id",nullable=false)
	private Integer idConsulta;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime fecha;
	
	@ManyToOne
	@JoinColumn(name="id_paciente",nullable=false)
	private Paciente paciente;
	@ManyToOne
	@JoinColumn(name="id_medico",nullable=false)	
	private Medico medico;
	@ManyToOne
	@JoinColumn(name="id_especialidad",nullable=false)	
	private Especialidad especialidad;
	
	@OneToMany(mappedBy="cons", //Aqui mapeamos con el atributo que hace referencia a la clase maestra dentro de la clase detalle
			   cascade = {CascadeType.PERSIST,
					   	  CascadeType.MERGE,
					   	  CascadeType.REMOVE},
			   fetch=FetchType.LAZY,
			   orphanRemoval = true)
	private List<DetalleConsulta> detalleConsulta;
	
	
	
	public Integer getIdConsulta() {
		return idConsulta;
	}
	public void setIdConsulta(Integer idConsulta) {
		this.idConsulta = idConsulta;
	}
	public LocalDateTime getFecha() {
		return fecha;
	}
	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}
	public Paciente getPaciente() {
		return paciente;
	}
	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
	public Medico getMedico() {
		return medico;
	}
	public void setMedico(Medico medico) {
		this.medico = medico;
	}
	public Especialidad getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(Especialidad especialidad) {
		this.especialidad = especialidad;
	}
	
	public List<DetalleConsulta> getDetalleConsulta() {
		return detalleConsulta;
	}
	public void setDetalleConsulta(List<DetalleConsulta> detalleConsulta) {
		this.detalleConsulta = detalleConsulta;
	}
}

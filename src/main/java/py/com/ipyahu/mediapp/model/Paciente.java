package py.com.ipyahu.mediapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size; //javax.validation.constraint PAQUETE DE VALIDACION

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description="Informacion del Paciente")
@Entity
@Table(name="pacientes")
public class Paciente {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Integer idPaciente;
	
	@ApiModelProperty(notes="Nombres debe tener entre 3 y 100 caracteres.")
	@Size(min = 3, max = 100, message = "Nombres debe tener entre 3 y 100 caracteres.") 
	@Column(name="nombres", nullable = false, length = 100)
	private String nombres;
	
	@ApiModelProperty(notes="Apellidos debe tener entre 3 y 100 caracteres.")
	@Size(min = 3, max = 100, message = "Apellidos debe tener entre 3 y 100 caracteres.")
	@Column(name="apellidos", nullable = false, length = 100)	
	private String apellidos;
	
	@ApiModelProperty(notes="C.I. debe tener entre 3 y 20 caracteres.")
	@Size(min = 3, max = 20, message = "C.I. debe tener entre 3 y 20 caracteres.")
	@Column(name="ci", nullable = false, length = 20)	
	private String ci;
	
	@ApiModelProperty(notes="Direccion puede tener hasta 100 caracteres.")
	@Size(max = 100, message = "Direccion puede tener hasta 100 caracteres.")
	@Column(name="direccion", nullable = true, length = 100)	
	private String direccion;
	
	@ApiModelProperty(notes="Telefono puede tener hasta 100 caracteres.")
	@Size(max = 20, message = "Telefono puede tener hasta 100 caracteres.")
	@Column(name="telefono", nullable = true, length = 20)
	private String telefono;
	
	@ApiModelProperty(notes="Email puede tener hasta 30 caracteres.")
	@Size(max= 30, message="Email puede tener hasta 30 caracteres.")
	@Column(name="email",nullable= true, length=30)
	private String email;
	
	public Integer getIdPaciente() {
		return idPaciente;
	}
	public void setIdPaciente(Integer idPaciente) {
		this.idPaciente = idPaciente;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getCi() {
		return ci;
	}
	public void setCi(String ci) {
		this.ci = ci;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}

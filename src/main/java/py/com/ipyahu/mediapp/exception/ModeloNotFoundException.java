package py.com.ipyahu.mediapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ModeloNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 3768714565277569864L;

	public ModeloNotFoundException(String mensaje){
		super(mensaje);
	}
}
